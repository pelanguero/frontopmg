import React, { Component } from "react";
import ReactDOM from "react-dom";
import Header from "../components/Header";
import optlogo from "../sources/OPTICAS-Lic-MArcela-Gonzales-0001.png";
class Home extends React.Component {
  render() {
    return <Header logo={optlogo}> </Header>;
  }
}
export default Home;
