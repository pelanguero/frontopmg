import Home from "./vistas/Home";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "./App.css";

function App() {
  return (
    <Router>
      <Route path="/">
        <Home></Home>
      </Route>
    </Router>
  );
}

export default App;
