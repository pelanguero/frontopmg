import React, { Component } from "react";
import ReactDOM from "react-dom";
import { Link } from "react-router-dom";
import "../css/header.css";
class Header extends React.Component {
  // propiedades,redes sociales numero whatsapp y correo de contacto
  // estado si el usuario esta logueado o no
  constructor(props) {
    super(props);
    this.state = {
      items: 0,
    };
  }
  render() {
    return (
      <header id="headd">
        <div id="overHead"></div>
        <div id="navHeader">
          <div id="divlogo">
            <Link to="/">
              <img id="logo" src={this.props.logo} alt="logo"></img>
            </Link>
          </div>
          <nav id="navnav">
            <ul id="subnavnav">
              <li class="navItem">
                <Link class="naItem">Catalogo</Link>
              </li>
              <li class="navItem">
                <Link class="naItem">Nuevo</Link>
              </li>
              <li class="navItem">
                <Link class="naItem">Accesorios</Link>
              </li>
              <li class="navItem">
                <Link class="naItem">Blog</Link>
              </li>
            </ul>
          </nav>
          <div id="carroBusqueda">
            <div id="lupa">
              <svg
                alt="busqueda"
                version="1.1"
                id="llupa"
                xmlns="http://www.w3.org/2000/svg"
                xlink="http://www.w3.org/1999/xlink"
                x="0px"
                y="0px"
                width="475.084px"
                height="475.084px"
                viewBox="0 0 475.084 475.084"
                space="preserve"
              >
                <g>
                  <path
                    d="M464.524,412.846l-97.929-97.925c23.6-34.068,35.406-72.047,35.406-113.917c0-27.218-5.284-53.249-15.852-78.087
		c-10.561-24.842-24.838-46.254-42.825-64.241c-17.987-17.987-39.396-32.264-64.233-42.826
		C254.246,5.285,228.217,0.003,200.999,0.003c-27.216,0-53.247,5.282-78.085,15.847C98.072,26.412,76.66,40.689,58.673,58.676
		c-17.989,17.987-32.264,39.403-42.827,64.241C5.282,147.758,0,173.786,0,201.004c0,27.216,5.282,53.238,15.846,78.083
		c10.562,24.838,24.838,46.247,42.827,64.234c17.987,17.993,39.403,32.264,64.241,42.832c24.841,10.563,50.869,15.844,78.085,15.844
		c41.879,0,79.852-11.807,113.922-35.405l97.929,97.641c6.852,7.231,15.406,10.849,25.693,10.849
		c9.897,0,18.467-3.617,25.694-10.849c7.23-7.23,10.848-15.796,10.848-25.693C475.088,428.458,471.567,419.889,464.524,412.846z
		 M291.363,291.358c-25.029,25.033-55.148,37.549-90.364,37.549c-35.21,0-65.329-12.519-90.36-37.549
		c-25.031-25.029-37.546-55.144-37.546-90.36c0-35.21,12.518-65.334,37.546-90.36c25.026-25.032,55.15-37.546,90.36-37.546
		c35.212,0,65.331,12.519,90.364,37.546c25.033,25.026,37.548,55.15,37.548,90.36C328.911,236.214,316.392,266.329,291.363,291.358z
		"
                  />
                </g>
              </svg>
            </div>
            <div id="carro">
              <div id="aucarrou">
                <div id="aucarrod">
                  <svg
                    version="1.1"
                    alt="cart"
                    id="cart"
                    xmlns="http://www.w3.org/2000/svg"
                    xlink="http://www.w3.org/1999/xlink"
                    x="0px"
                    y="0px"
                    viewBox="0 0 122.9 107.5"
                    space="preserve"
                  >
                    <g>
                      <path d="M3.9,7.9C1.8,7.9,0,6.1,0,3.9C0,1.8,1.8,0,3.9,0h10.2c0.1,0,0.3,0,0.4,0c3.6,0.1,6.8,0.8,9.5,2.5c3,1.9,5.2,4.8,6.4,9.1 c0,0.1,0,0.2,0.1,0.3l1,4H119c2.2,0,3.9,1.8,3.9,3.9c0,0.4-0.1,0.8-0.2,1.2l-10.2,41.1c-0.4,1.8-2,3-3.8,3v0H44.7 c1.4,5.2,2.8,8,4.7,9.3c2.3,1.5,6.3,1.6,13,1.5h0.1v0h45.2c2.2,0,3.9,1.8,3.9,3.9c0,2.2-1.8,3.9-3.9,3.9H62.5v0 c-8.3,0.1-13.4-0.1-17.5-2.8c-4.2-2.8-6.4-7.6-8.6-16.3l0,0L23,13.9c0-0.1,0-0.1-0.1-0.2c-0.6-2.2-1.6-3.7-3-4.5 c-1.4-0.9-3.3-1.3-5.5-1.3c-0.1,0-0.2,0-0.3,0H3.9L3.9,7.9z M96,88.3c5.3,0,9.6,4.3,9.6,9.6c0,5.3-4.3,9.6-9.6,9.6 c-5.3,0-9.6-4.3-9.6-9.6C86.4,92.6,90.7,88.3,96,88.3L96,88.3z M53.9,88.3c5.3,0,9.6,4.3,9.6,9.6c0,5.3-4.3,9.6-9.6,9.6 c-5.3,0-9.6-4.3-9.6-9.6C44.3,92.6,48.6,88.3,53.9,88.3L53.9,88.3z M33.7,23.7l8.9,33.5h63.1l8.3-33.5H33.7L33.7,23.7z" />
                    </g>
                  </svg>
                </div>
                <div id="numeroitems">{this.state.items}</div>
              </div>
            </div>
          </div>
        </div>
      </header>
    );
  }
}
export default Header;
